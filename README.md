# pysaml3.4-saml

This library add SAML support to the Python software. This version supports Python 3.6 only.

Installation
------------

### Dependencies ###

 * python 2.7 // python 3.6
 * [xmlsec](https://pypi.python.org/pypi/xmlsec) Python bindings for the XML Security Library.
 
```
$ pip install xmlsec
```

 * [isodate](https://pypi.python.org/pypi/isodate) An ISO 8601 date/time/
 duration parser and formatter

```
$ pip install isodate
```

 * [defusedxml](https://pypi.python.org/pypi/defusedxml) XML bomb protection for Python stdlib modules

```
$ pip install defusedxml
``` 

Configuration
------------

#### Set the metadata, sso, slo, crt, key, pem parameters in settings.json ####

* [settings.json](saml/settings.json) Update the values of the SP & IDP.

#### Upload the key, crt & key to certs folder ####

* Upload the key, crt & pem files to the ``/certs`` folder.

Integration
------------

Refer  to ``/demo-flask/index.py`` file on how to add SAML support to the Flask Framework. 

### Source Code ###

#### Download the toolkit from pypi ####

The toolkit is hosted in pypi, you can find the ``python3-saml`` package at https://pypi.python.org/pypi/python3-saml

```
$ pip install python3-saml
```

### Known Issues ###

#### Install xmlsec using the appropriate binary wheel ####

In Windows, you can install xmlsec using the binary wheel, please make sure you install using the appropriate version of Python.

 * [xmlsec-1.3.52.dev0-cp27-cp27m-win_amd64.whl](resources/xmlsec-1.3.52.dev0-cp27-cp27m-win_amd64.whl) Python 2.7.x
  
 * [xmlsec-1.3.52.dev0-cp35-cp35m-win_amd64.whl](resources/xmlsec-1.3.52.dev0-cp35-cp35m-win_amd64.whl) Python 3.5.x

 * [xmlsec-1.3.52.dev0-cp36-cp36m-win_amd64.whl](resources/xmlsec-1.3.52.dev0-cp36-cp36m-win_amd64.whl) Python 3.6.x
